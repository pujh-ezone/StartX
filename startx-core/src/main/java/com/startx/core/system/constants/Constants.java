package com.startx.core.system.constants;

public class Constants {
	
	/**
	 * uri
	 */
	public static final String ROOT_DIR = "/";
	public static final String FAVICON = "/favicon.ico";
	public static final String PARAM_SPLIT = "?";
	public static final String EMPTY_STRING = "";
	public static final String UNDER_LINE = "_";
	public static final String DOT = ".";
	/**
	 * 冒号
	 */
	public final static String COLON = ":";
	
	/**
	 * xml
	 */
	public static final String START_XML = "<xml>";
	public static final String END_XML = "</xml>";
	
	/**
	 * any address
	 */
	public static final String ANY_ADDRESS = "0.0.0.0";
	/**
	 * 服务版本
	 */
	public static final String SERVER_VERSION = "StartX/1.3.1";
}
