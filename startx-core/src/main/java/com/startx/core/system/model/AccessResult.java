package com.startx.core.system.model;

/**
 * 访问响应
 * 
 * @author minghu.zhang
 */
public class AccessResult {

	/**
	 * endpoint
	 */
	private AccessTarget target;
	/**
	 * 获取到结果
	 */
	private Object result;
	
	public AccessResult(AccessTarget target, Object result) {
		super();
		this.target = target;
		this.result = result;
	}

	public AccessTarget getTarget() {
		return target;
	}

	public void setTarget(AccessTarget target) {
		this.target = target;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
