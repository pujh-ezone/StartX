package com.startx.core.system.param;

import java.util.Objects;

import com.startx.core.tools.MD5;

import io.netty.channel.ChannelHandlerContext;

/**
 * HTTP上下文
 * 
 * @author minghu.zhang
 */
public class HttpContext {
	/**
	 * ip地址
	 */
	private String ip;
	/**
	 * 请求ID
	 */
	private String requestId;
	/**
	 * 管道上下文
	 */
	private ChannelHandlerContext context;

	public HttpContext(String ip, ChannelHandlerContext context) {
		super();
		this.ip = ip;
		this.context = context;
	}

	public String getIp() {
		return ip;
	}

	public ChannelHandlerContext getContext() {
		return context;
	}

	public String getRequestId() {
		
		if(Objects.isNull(requestId)) {
			requestId = MD5.encrypt16(context.channel().id().asLongText());
		}
		
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setContext(ChannelHandlerContext context) {
		this.context = context;
	}

}
