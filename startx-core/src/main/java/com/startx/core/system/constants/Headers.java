package com.startx.core.system.constants;

import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpHeaders;

/**
 * 公共header
 */
public class Headers {
	
	private static final HttpHeaders JSON_HEADERS = new DefaultHttpHeaders();
	
	private static final HttpHeaders XML_HEADERS = new DefaultHttpHeaders();

	public static final  HttpHeaders RESOURCE_HEADERS = new DefaultHttpHeaders();
	
	public static final  HttpHeaders COMMON_HEADERS = new DefaultHttpHeaders();
	
	/**
	 * 默认Header值
	 * @author minghu.zhang
	 */
	public static class Values {
		public static final String AcceptEncoding = "Accept-Encoding";
		public static final String DefaultAcceptEncoding = "identity";
	}

	// 静态资源
	static {
		RESOURCE_HEADERS.add("Access-Control-Allow-Origin", "*");
		RESOURCE_HEADERS.add("Cache-Control","max-age=315360000, public, immutable");
		RESOURCE_HEADERS.add("Server",Constants.SERVER_VERSION);
		RESOURCE_HEADERS.add("StartX", "https://gitee.com/sharegpj/StartX");
	}

	/**
	 * json
	 */
	static {
		JSON_HEADERS.add("Access-Control-Allow-Origin", "*");
		JSON_HEADERS.add("Content-Type", "application/json;charset=utf-8");
		JSON_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		JSON_HEADERS.add("Server",Constants.SERVER_VERSION);
		JSON_HEADERS.add("Pragma", "no-cache");
	}
	
	/**
	 * xml
	 */
	static {
		XML_HEADERS.add("Access-Control-Allow-Origin", "*");
		XML_HEADERS.add("Content-Type", "text/xml;charset=utf-8");
		XML_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		XML_HEADERS.add("Server",Constants.SERVER_VERSION);
		XML_HEADERS.add("Pragma", "no-cache");
	}
	
	/**
	 * common
	 */
	static {
		COMMON_HEADERS.add("Access-Control-Allow-Origin", "*");
		COMMON_HEADERS.add("Content-Type", "text/plain;charset=utf-8");
		COMMON_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		COMMON_HEADERS.add("Server",Constants.SERVER_VERSION);
		COMMON_HEADERS.add("Pragma", "no-cache");
	}

	public static HttpHeaders getJsonHeader() {
		return JSON_HEADERS;
	}

	public static HttpHeaders getResourceHeader() {
		return RESOURCE_HEADERS;
	}
	
	public static HttpHeaders getXmlHeader() {
		return XML_HEADERS;
	}
	
	public static HttpHeaders getCommonHeader() {
		return COMMON_HEADERS;
	}
}
