package com.startx.core;

import com.startx.core.config.ConfigHolder;
import com.startx.core.filter.chain.FilterFactory;
import com.startx.core.mvc.ApplicationContext;
import com.startx.core.netty.server.Server;

/**
 * 服务启动入口
 */
public class Bootstrap {
	
	/**
	 * 启动服务
	 */
	public static void start() throws Exception {
		//读取配置
		ConfigHolder.initConfig();
		// 初始化AccessPoint，初始化Spring
		ApplicationContext.initMapping();
		//初始化过滤器
		FilterFactory.initFilter();
		// 启动NettyServer
		Server.start();
	}
}
