package com.startx.core.filter;

import com.startx.core.system.model.AccessResult;
import com.startx.core.system.model.AccessTarget;
import com.startx.core.system.param.HttpContext;

import io.netty.handler.codec.http.FullHttpRequest;

/**
 * 过滤器
 * @author minghu.zhang
 */
public interface StartxFilter {
	
	/**
	 * 执行之前，true 表示继续执行，false表示结束
	 * @param ctx
	 * @param request
	 * @param target
	 * @return
	 */
	public boolean doBefore(HttpContext ctx, FullHttpRequest request, AccessTarget target) throws Exception;
	
	/**
	 * 执行之后，true 表示继续执行，false表示结束
	 * @param ctx
	 * @param request
	 * @param target
	 * @return
	 */
	public boolean doAfter(HttpContext ctx, AccessResult result) throws Exception;
}
