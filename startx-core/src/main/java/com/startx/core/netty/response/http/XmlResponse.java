package com.startx.core.netty.response.http;

import java.io.UnsupportedEncodingException;

import com.startx.core.system.constants.Headers;
import com.startx.core.tools.xml.XmlWriter;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * http短连接数据返回
 */
public class XmlResponse {

	/**
	 * 返回Object，默认转为json
	 * 
	 * @param ctx
	 * @param status
	 * @param result
	 * @throws UnsupportedEncodingException
	 */
	public static FullHttpResponse getResponse(HttpResponseStatus status, Object result,
			HttpHeaders... headers) throws UnsupportedEncodingException {
		ByteBuf buffer = Unpooled.wrappedBuffer(XmlWriter.startXml().writeObject(result).endXml().getBytes("utf-8"));
		FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, buffer);
		setPublicHeader(response, headers);
		return response;
	}

	/**
	 * 设置公共header
	 * 
	 * @param res
	 */
	private static void setPublicHeader(FullHttpResponse res, HttpHeaders... headers) {
		res.headers().add(Headers.getXmlHeader());
		for (HttpHeaders header : headers) {
			res.headers().add(header);
		}
	}
}
