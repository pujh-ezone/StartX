package com.startx.core.netty.handler;

import com.startx.core.netty.response.socket.SocketOutput;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class SocketHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		
		String request = (String) msg;
		//处理response
		
		//可以基于json处理
		
		//返回数据
		SocketOutput.bytes(ctx, ("received:"+request+".\r\n\r\n").getBytes());
	}	
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		//connected.
		SocketOutput.bytes(ctx, "connected.\r\n\r\n".getBytes());
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		//disconnected.
		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
	
	
}
