package com.startx.core.netty.response.http;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * http短连接数据返回
 */
public class RedirectResponse {
	
	/**
	 * 302跳转
	 * @param target 跳转目标
	 */
	public static void sendRedirect(ChannelHandlerContext ctx,String target) {
		FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,HttpResponseStatus.FOUND);
		response.headers().set("location",target==null?"/":target.toString());
		ChannelFuture f = ctx.channel().writeAndFlush(response);
		f.addListener(ChannelFutureListener.CLOSE);
    }
	
}
