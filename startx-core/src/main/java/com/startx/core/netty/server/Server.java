package com.startx.core.netty.server;

import com.startx.core.config.ConfigHolder;

/**
 * netty_server
 */
public abstract class Server {
	
	private static Server server;
	static {
		try {
			server = (Server) Class.forName(ConfigHolder.getConfig().getServer()).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("初始化ServerClass异常，请配置正确的Sever类");
		}
	}

	/**
	 * 启动服务器
	 * 
	 * @throws Exception
	 */
	protected abstract void run() throws Exception;

	/**
	 * 启动服务
	 * 
	 * @throws Exception
	 */
	public static void start() throws Exception {
		server.run();
	}
	
}