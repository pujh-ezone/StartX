
package com.startx.core.netty.response.socket;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

public class SocketOutput {

	/**
	 * 返回数据
	 * @param ctx
	 * @param bytes
	 */
	public static void bytes(
            ChannelHandlerContext ctx, byte[] bytes) {
        ctx.channel().writeAndFlush(Unpooled.wrappedBuffer(bytes));
    }
}
