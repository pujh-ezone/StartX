package com.startx.core.mvc.define;

/**
 * 请求方法枚举
 */
public enum RequestMethod {
	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE, WEBSOCKET
}
