package com.startx.core.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 读取MimeType
 * @author minghu.zhang
 */
public class MimeType {

	
	//type dic
	private static final Map<String,String> mimeTypes = new HashMap<>();
	//init
	static {
		
		try {
			InputStream input = MimeType.class.getResourceAsStream("/mimeType.types");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			String line = "";
			
			while((line = reader.readLine()) != null) {
				
				String extension = line.substring(0, line.indexOf(" "));
				String mimeType  = line.substring(line.lastIndexOf(" ")+1);
				
				mimeTypes.put(extension, mimeType);
			}
			
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取mimeType
	 * @param extension
	 * @return
	 */
	public static final String getMimeType(String extension) {
		return mimeTypes.get(extension);
	}
}
