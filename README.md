# StartX （已更新至v1.3.10）


<br/>

### 项目简介  

<br/>

基于Netty和Spring开发的服务端MVC容器，支持Socket，WebSocket(SSL)，HTTP(S)，提供集成环境，能很快速的进入业务开发，对业务开发者透明，支持对各种通讯协议进行定制

<br/>

### 后期计划


<br/>

- 针对V1.3.1的Bug修复（主要工作）
- 开发规范定义，代码优化重构
- 新版本2.x开发（包含微服务模块）
- 基于StartX的推送服务
- 基于StartX的即时通讯

欢迎有兴趣的小伙伴跟我联系，一起完成后期设计和开发工作，也欢迎通过Q群联系作者，Q群：781722679
<br/>

<br/>


### 项目打包

<br/>

#### 打包配置

<br/>

	- pom.xml 配置assembly打包插件以及项目打jar包需要排除的配置文件
	- assembly.xml 配置assembly打包脚本，这里主要关注需要打包到jar之外的配置文件得include

<br/>

#### 关键代码

<br/>

- pom.xml
<br/>

```
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-jar-plugin</artifactId>
	<version>2.4</version>
	<configuration>
		<!-- 注意添加排除目录到打包插件 -->
		<excludes>
			<exclude>**/logs/</exclude>
			<exclude>**/*.xml</exclude>
			<exclude>**/*.properties</exclude>
			<exclude>**/*.sh</exclude>
			<exclude>config</exclude>
		</excludes>
	</configuration>
</plugin>

<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-assembly-plugin</artifactId>
	<version>2.4</version>
	<configuration>
		<descriptors>
			<descriptor>src/main/assembly/assembly.xml</descriptor>
		</descriptors>
	</configuration>
	<executions>
		<execution>
			<id>make-assembly</id>
			<phase>package</phase>
			<goals>
				<goal>single</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```

<br/>

- assembly.xml

<br/>

```
<assembly>
	<id>release</id>
	<formats>
		<format>tar.gz</format>
	</formats>
	<dependencySets>
		<!-- refer to http://maven.apache.org/plugins/maven-assembly-plugin/assembly.html#class_dependencySet -->
		<dependencySet>
			<useProjectArtifact>true</useProjectArtifact>
			<outputDirectory>lib</outputDirectory>
		</dependencySet>
	</dependencySets>
	<fileSets>
		<fileSet>
			<directory>logs</directory>
			<outputDirectory>logs</outputDirectory>
		</fileSet>
		<fileSet>
			<directory>target/classes</directory>
			<outputDirectory>conf</outputDirectory>
			<includes>
				<!-- 添加配置文件注意配置此处加入conf目录 -->
				<include>*.xml</include>
				<include>*.properties</include>
				<include>*.jks</include>
				<include>config/*</include>
			</includes>
		</fileSet>
		<fileSet>
			<directory>src/main/scripts</directory>
			<outputDirectory>bin</outputDirectory>
			<fileMode>0733</fileMode>
			<includes>
				<include>*.sh</include>
			</includes>
		</fileSet>
		<fileSet>
			<directory>libs</directory>
			<outputDirectory>lib</outputDirectory>
			<includes>
				<include>*.jar</include>
			</includes>
		</fileSet>
	</fileSets>

</assembly>

```

<br/>

#### 启动脚本

<br/>

脚本文件startup.sh，实现了start，stop等命令，执行方式为：bin/startup.sh start|stop，这里关注两个配置项：

- 启动类（启动之后的进程名，也就是启动示例的J4，可以自己配置） 
- JVM 参数，根据自己的服务器配置或优化经验进行调整

具体脚本请参见startup.sh

<br/>

```

_ServerClass=com.startx.test.J4
JAVA_OPTS=" -server -Xms5120m -Xmx5120m -Xmn4096m -XX:MetaspaceSize=128M -verbose:gc -Xloggc:logs/gc.log -XX:+PrintGCTimeStamps 
-XX:+PrintGCDetails -XX:+PrintTenuringDistribution -XX:MaxTenuringThreshold=10 -XX:SurvivorRatio=8 -XX:PermSize=128m -XX:MaxPermSize=128m 
-XX:+UseParNewGC  -Xss1024k"

```
<br/>

#### 执行编译

<br/>

mvn clean install，得到xxx.tar.gz压缩包

<br/>
解压之后结构如下：

<br/>

```
├─bin                  #启动脚本
├─etc                  #项目配置文件
│  └─config            #项目Spring相关配置文件
└─lib                  #项目依赖包
```

<br/>

#### 部署服务

<br/>

- 上传代码到安装有jdk环境的Linux服务器
- 执行代码转换脚本为unix格式: dos2unix bin/startup.sh
- 启动程序或停止程序：bin/startup.sh start|stop
- 会自动生成logs文件夹，日志内容为控制台输出日志
- 当前自动生成的pid文件请勿删除，以防重复启动
- 使用jps查看进程，能看到J4进程正在运行

<br/>

#### 注意事项

<br/>

请修改为当前合适的配置，代码已移除具体配置，例如Mysql地址。本框架不适用于页面网站开发，框架定位于API服务，游戏后端服务，数据统计服务等场景

<br/>

### Demo示例

<br/>

#### 引入startx-http-demo 

<br/>

#### 打开GameApp，右键运行

<br/>

#### 调用接口（框架基于mongodb+mysql+redis）

<br/>

- 调用数据存储

```

http://localhost/data/save?token=&cover=               

token：用户令牌，通过令牌存储该用户数据

Cover：是否覆盖，值任意，当cover有值时，当前json会完整覆盖用户数据，否则只会覆盖或添加某个字段

Method:POST

Body

{
   "a":1,
   "b":[
        1,2,3,4,5 
    ]
}

Response

{
    "code": {
        "msg": "successful",
        "code": 200
    },
    "data": {}
}

```

- 调用数据获取

```

http://localhost/data/get?token=token&key= 

token：用户令牌，通过令牌获取该用户数据

key：需要获取的数据字段，若key为空，则获取该令牌下的所有数据

Method:GET

Response

举例：token=token

{
    "code": {
        "msg": "successful",
        "code": 200
    },
    "data": {
        "a": 1,
        "b": [
            1,
            2,
            3,
            4,
            5
        ]
    }
}

举例：token=token&key=b
{
    "code": {
        "msg": "successful",
        "code": 200
    },
    "data": [
        1,
        2,
        3,
        4,
        5
    ]
    
}

```
