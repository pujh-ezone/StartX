package com.startx.http.dao;

import java.util.List;

import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * 基本操作接口
 * @author minghu.zhang
 */
public interface IMongodBaseDao<T> {
	/**
	 * 保存对象
	 */
	public void save(T t);
	
	/**
	 * 保存对象
	 */
	public void insert(T t);
	
	/**
	 * 保存一组对象
	 */
	public void insertAll(List<T> ts);
	
	/**
	 * 保存对象
	 */
	public void saveObject(Object t);
	
	/**
	 * 保存对象
	 */
	public void insertObject(Object t);
	
	/**
	 * 保存一组对象
	 */
	public void insertAllObject(List<?> os);
	
	/**
	 * 根据ID获取数据
	 */
	public T getById(Object id);
	
	/**
	 * 根据ID获取数据
	 */
	public <N extends Object>N getById(Object id,Class<?> clz);

	/**
	 * 根据ID获取数据
	 */
	public void removeById(Object id);
	
	/**
	 * 根据ID获取数据
	 */
	public void removeById(Object id,Class<?> clz);
	
	/**
	 * 获取mongo模板
	 * @return
	 */
	public MongoTemplate getMongoTemplate();
	
	/**
	 * 获取下一个ID
	 * @param step 递增数 小于等于0将使用随机值递增
	 * @return
	 */
	public long getNextSequenceId(int step);
	
	/**
	 * 获取下一个ID
	 * @param step 递增数 小于等于0将使用随机值递增
	 * @return
	 */
	public long getNextSequenceId(Class<?> clz,int step);
}
