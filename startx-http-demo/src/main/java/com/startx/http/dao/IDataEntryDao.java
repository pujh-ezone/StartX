package com.startx.http.dao;

import java.util.Map;

import com.startx.http.entity.data.DataEntry;

public interface IDataEntryDao extends IMongodBaseDao<DataEntry>  {

	/**
	 * 保存或更新
	 * @param entry
	 */
	public void saveOrUpdate(DataEntry entry);

	/**
	 * 获取某个字段
	 * @param userId
	 * @param key
	 * @return
	 */
	public Map<String,?> getKeyValueByKey(String userId, String key);

}
