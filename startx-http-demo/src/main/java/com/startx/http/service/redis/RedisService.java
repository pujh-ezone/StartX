package com.startx.http.service.redis;

import com.startx.http.service.IRedisService;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import javax.inject.Inject;
import java.util.*;

@Service
public class RedisService implements IRedisService {


	@Inject
	private JedisPool jedisPool;

	private Jedis getResource() {
		return jedisPool.getResource();
	}


	@Override
	public String saveString(String key, String value, int second) {
		Jedis jedis = getResource();
		String code = jedis.set(key, value);
		if (0 != second){
			jedis.expire(key, second);
		}
		return code;

	}

	@Override
	public String getString(String key) {
		Jedis jedis = getResource();
		return jedis.get(key);
	}

	@Override
	public Long delete(String key) {
		Jedis jedis = getResource();
		return jedis.del(key);
	}


	// -------------------- map操作 ------------------------------------

	@Override
	public List<String> getMapKeys(String key, String[] str) {
		Jedis jedis = getResource();
		return jedis.hmget(key, str);

	}

	@Override
	public Set<String> findMapKeys(String key) {
		Jedis jedis = getResource();
		return jedis.hkeys(key);
	}

	@Override
	public List<String> findMapValue(String key) {
		Jedis jedis = getResource();
		return jedis.hvals(key);
	}



	// ---------------- set方法的使用 ---------------------------------------

	@Override
	public void saveSet(String key, String... value) {
		Jedis jedis = getResource();
		jedis.sadd(key,value);

	}

	@Override
	public Set<String> getSet(String key) {
		Jedis jedis = getResource();
		Set<String> s = jedis.smembers(key);
		return s;
	}

	@Override
	public void delSet(String key, String value) {
		Jedis jedis = getResource();
		jedis.srem(key, value);
	}

	// 判断set中有没有这个值
	@Override
	public Boolean existsSetValue(String key, String value) {
		Jedis jedis = getResource();
		return jedis.sismember(key, value);
	}


	// ----------------------- 工具方法 -----------------------------------

	@Override
	public Long getKeyTime(String key) {
		Jedis jedis = getResource();
		Long l = jedis.ttl(key);
		return l;
	}


	@Override
	public Boolean existsKeys(String key) {
		Jedis jedis = getResource();
		return jedis.exists(key);
	}

	/**
	 * 保存一个值以list形式存储
	 */
	@Override
	public Long saveList(String key, String lstValue) {
		Jedis jedis = getResource();
		return jedis.lpush(key, lstValue);
	}

	/**
	 * 查询所有
	 */
	@Override
	public List<String> getList(String key) {
		Jedis jedis = getResource();
		List<String> list = jedis.lrange(key, 0, -1);
		return list;
	}

	/**
	 * 以map形式保存在缓存中，传一个map。
	 */
	@Override
	public String saveMap(String key, Map<String, String> map) {
		Jedis jedis = getResource();
		return jedis.hmset(key, map);
	}

	/**
	 * 直接获取一个map类型的key返回一个map集合。
	 */
	@Override
	public Map<String, String> getMap(String key) {
		Jedis jedis = getResource();
		return jedis.hgetAll(key);
	}

	@Override
	public String getMap(String key,String mKey) {
		Jedis jedis = getResource();
		String ss = jedis.hget(key, mKey);
		return ss;
	}

}
