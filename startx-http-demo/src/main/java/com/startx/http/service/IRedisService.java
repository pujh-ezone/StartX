package com.startx.http.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IRedisService {

	/**
	 * 根据key 获得一个value值
	 */
	public String getString(String key);

	/**
	 * 根据key保存和更新value
	 */
	public String saveString(String key, String value, int second);


	/**
	 * 根据key删除
	 */
	public Long delete(String key);


	// -------------------- map操作 ------------------------------------

	/**
	 * 可以传一个数组(数组里都是map里面的key),然后返回这些key里所对应的value。
	 */
	public List<String> getMapKeys(String key, String[] str) ;

	/**
	 * 返回所有的keys.只针对map类型的key.
	 */
	public Set<String> findMapKeys(String key);

	/**
	 * 返回所有的value.只正对map类型的key.
	 */
	public List<String> findMapValue(String key) ;

	public String saveMap(String key, Map<String, String> map) ;

	public Map<String, String> getMap(String key);
	/**
	 * 根据mKey 获取map，
	 * @param key
	 * @param mKey
	 * @return
	 */
	public String getMap(String key,String mKey);


	// ---------------- set方法的使用 ---------------------------------------

	/**
	 * 保存一个key以set形式保存
	 */
	public void saveSet(String key, String... value) ;

	/**
	 * 获取指定key下set的所有元素。
	 */
	public Set<String> getSet(String key) ;

	/**
	 * 删除指定值
	 */
	public void delSet(String key, String value) ;

	/**
	 * 判断一个key 下有没有这个值。
	 */
	public Boolean existsSetValue(String key, String value) ;


	public Long getKeyTime(String key) ;

	public Boolean existsKeys(String key) ;

	public Long saveList(String key, String lstValue);

	public List<String> getList(String key);


}
