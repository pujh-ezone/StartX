package com.startx.http.service;

import java.util.Map;

import com.startx.core.system.param.HttpBody;
import com.startx.core.system.param.HttpParam;

public interface IDataEntryService {

	/**
	 * 保存数据
	 * @param param
	 * @param body
	 */
	public void doSaveDataEntry(HttpParam param, HttpBody body);

	/**
	 * 获取数据
	 * @param param
	 * @return
	 */
	public Map<String,?> doGetDataEntry(HttpParam param);

}
