package com.startx.http.system.quartz;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 榜单更新
 * @author minghu.zhang
 */
@Component
public class SequenceTimer {
	
	/**
	 * 通用文本
	 */
//	private ResourceManager COMMON = TextTemplate.COMMON;

	/**
	 * 执行刷新间隔
	 */
	@Scheduled(cron="0/3 * * * * ?")
	public void dodo() {
//		System.out.println(System.currentTimeMillis());
	}
}
