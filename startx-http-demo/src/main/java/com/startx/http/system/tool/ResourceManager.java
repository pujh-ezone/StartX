package com.startx.http.system.tool;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Manager class to get localized resources.
 */
public class ResourceManager {
	//Resource Bundle
	private ResourceBundle bundle;

	private ResourceManager(String baseName, Locale locale) {
		this.bundle = ResourceBundle.getBundle(baseName, locale);
	}

	public static ResourceManager getInstance(String baseName) {
		return new ResourceManager(baseName, Locale.getDefault());
	}

	public static ResourceManager getInstance(String baseName, Locale locale) {
		return new ResourceManager(baseName, locale);
	}

	public String getString(String key, Object... args) {

		if (args.length > 0) {
			return MessageFormat.format(bundle.getString(key), args);
		} else {
			return bundle.getString(key);
		}

	}
}
