package com.startx.http.system.tool;

import com.startx.http.system.encrypt.aes.AES;

/**
 * 密码签名工具
 * @author minghu.zhang
 */
public class Telephone {

	/**
	 * 密码签名前缀
	 */
	private static final String PREFIX = "[^-^]_[^-^]_[^-^]_[^-^]_[^-^]_[^-^]";

	/**
	 * 加密手机号
	 * @param password
	 * @return	
	 */
	public static String encrypt(String telephone,long userId) {
		return AES.encrypt(telephone,PREFIX+userId);
	}
	
	public static String decrypt(String telephone,long userId) {
		return AES.decrypt(telephone, PREFIX+userId);
	}
	
	
}
