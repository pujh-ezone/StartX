package com.startx.http.system.tool;

import com.google.common.collect.Maps;

import java.util.Map;

public class MapTool {
	
	/**
	 * 获取参数Map
	 * @param objects
	 * @return
	 */
	public static Map<Object,Object> getMap(Object ...objects) {
		Map<Object, Object> values = Maps.newHashMap();
		for (int i=0;i<objects.length;i+=2) {
			values.put(objects[i], objects[i+1]);
		}
		return values;
	}
	
}
