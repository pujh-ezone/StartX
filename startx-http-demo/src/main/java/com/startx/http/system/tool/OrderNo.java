package com.startx.http.system.tool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 订单号生成工具
 * @author minghu.zhang
 */
public class OrderNo {

	private static final SimpleDateFormat YDateFormat = new SimpleDateFormat("yyyyMMdd");
	/**
	 * 获取提现单号
	 * @return
	 */
	public static String getPaymentNo() {
		return getLotsNo(3);
	}
	/**
	 * 获取退款单号
	 * @return
	 */
	public static String getRefundNo() {
		return getLotsNo(2);
	}
	/**
	 * 获取订单号
	 * @return
	 */
	public static String getOrderNo() {
		return getLotsNo(1);
	}
	
	/**
	 * 获取各种单号
	 * @param type
	 * @return
	 */
	private static String getLotsNo(int type) {
		int hashCodev = UUID.randomUUID().toString().hashCode();
		if (hashCodev < 0) {
			// 有可能是负数
			hashCodev = -hashCodev;
		}
		Date now = new Date();
		String date = YDateFormat.format(now);
		// "%012d"的意思：0代表不足位数的补0，这样可以确保相同的位数，12是位数也就是要得到到的字符串长度是12，d代表数字。
		
		StringBuffer buffer = new StringBuffer();
		buffer
		.append(type)
		.append(String.format("%012d", hashCodev))
		.append(date)
		.append(RandomStr.getRandomStr(7, 0));
		return buffer.toString();
	}
}
