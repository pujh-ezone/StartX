package com.startx.http.system.setting;

import com.startx.core.tools.ConfigTool;

/**
 * 安全配置
 * @author minghu.zhang
 */
public class SecureSetting {
	
	/**
	 * 单例配置对象
	 */
	private static SecureSetting secureConfig; 
	
	static {
		secureConfig = new ConfigTool().read("/config.properties", SecureSetting.class);
	}
	
	/**
	 * 字符串解密
	 */
	private String aesDb;
	/**
	 * 短信AK/SK加解密
	 */
	private String aesSms;

	public static SecureSetting getSecureConfig() {
		return secureConfig;
	}

	public String getAesDb() {
		return aesDb;
	}

	public void setAesDb(String aesDb) {
		this.aesDb = aesDb;
	}

	public String getAesSms() {
		return aesSms;
	}

	public void setAesSms(String aesSms) {
		this.aesSms = aesSms;
	}
}
