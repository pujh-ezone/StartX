package com.startx.http.system.tool;

import org.springframework.util.StringUtils;

import com.google.common.collect.Range;
import com.startx.http.system.constant.Constants;

/**
 * 替换unicode工具
 * @author minghu.zhang
 */
public class Utf84byte {

	static Range<Character> utf84ByteRange = Range.closed("\u1F601".charAt(0), "\u1F64F".charAt(0));

	// 把utf-8字节编码的字符去掉
	// 参见: http://cenalulu.github.io/linux/character-encoding/
	// 所谓Emoji就是一种在Unicode位于\u1F601-\u1F64F区段的字符
	public static String replaceUtf84byte(String str) {
		if (StringUtils.isEmpty(str)) {
			return Constants.EMPTY_STRING;
		}

		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (Character.isSurrogate(chars[i])) { // 参见:
				// http://stackoverflow.com/questions/14981109/checking-utf-8-data-type-3-byte-or-4-byte-unicode
				chars[i] = ' '; // 替换成空格
			} else if (utf84ByteRange.contains(chars[i])) {
				chars[i] = ' '; // 替换成空格
			}
		}
		
		return new String(chars);
	}

}
