package com.startx.http.system.model;

import com.startx.core.system.param.HttpContext;
import io.netty.util.concurrent.FastThreadLocal;

public class ThreadContext {
	
	private static FastThreadLocal<HttpContext> LocalContext = new FastThreadLocal<>();
	
	public static void set(HttpContext context) {
		LocalContext.set(context);
	}
	
	public static HttpContext get() {
		return LocalContext.get();
	}
	
	public static void remove() {
		LocalContext.remove();
	}
	
}	
