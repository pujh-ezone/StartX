package com.startx.http.system.encrypt.hmacsha1;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;  
import javax.crypto.SecretKey;  
import javax.crypto.spec.SecretKeySpec;  
  
public class HMACSha1 {  
  
    private static final String MAC_NAME = "HmacSHA1";    
    private static final String ENCODING = "UTF-8";    
      
    /**  
     * 使用 HMAC-SHA1 签名方法对对encryptText进行签名  
     * @param encryptText 被签名的字符串  
     * @param encryptKey  密钥  
     * @throws Exception  
     */    
    public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey)   
    {           
        try {
			byte[] data=encryptKey.getBytes(ENCODING);  
			SecretKey secretKey = new SecretKeySpec(data, MAC_NAME);   
			Mac mac = Mac.getInstance(MAC_NAME);   
			mac.init(secretKey);    
			  
			byte[] text = encryptText.getBytes(ENCODING);    
			return mac.doFinal(text);
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException
				| IllegalStateException e) {
			e.printStackTrace();
			return new byte[]{};
		}    
    }    
} 