package com.startx.http.system.model;

/**
 * 令牌值
 * 
 * @author minghu.zhang
 */
public class TokenPair {

	private String token;
	private long   userId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public TokenPair(String token,long userId) {
		this.token = token;
		this.userId = userId;
	}
}
