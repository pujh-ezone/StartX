package com.startx.http.system.tool;

import com.startx.http.system.encrypt.md5.MD5;

/**
 * 密码签名工具
 * @author minghu.zhang
 */
public class Password {

	/**
	 * 密码签名前缀
	 */
	private static final String PREFIX = "[^_^]_[^_^]_[^_^]_[^_^]_[^_^]_[^_^]";

	/**
	 * 生成密码签名
	 * @param password
	 * @return	
	 */
	public static String get(String password) {
		return MD5.encrypt(PREFIX+password);
	}
	
}
