package com.startx.http.system.wallet;

import com.startx.http.system.encrypt.md5.MD5;

/**
 * 钱包和MD5值
 * 
 * @author minghu.zhang
 */
public class AmountMd5 {
	
	/**
	 * 密码签名前缀
	 */
	private static final String PREFIX = "[^=^]=[^=^]=[^=^]=[^=^]=[^=^]=[^=^]";

	/**
	 * 获取余额MD5
	 */
	public static String get(int amount,long userId) {
		return MD5.encrypt(amount+PREFIX+userId);
	}
	
}
