package com.startx.http.system.exception;

import com.startx.http.system.tool.ResponseBuilder.Code;

/**
 * 异常类定义
 * @author minghu.zhang
 */
@SuppressWarnings("serial")
public class MiniException extends RuntimeException {
	
	private Code code;
	
	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public MiniException(Code code) {
		this.code = code;
	}

	public MiniException(Code code,Throwable cause) {
		super(cause);
		this.code = code;
	}
}
