package com.startx.http.system.tool;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.bson.Document;

/**
 * BasicField构建
 * @author minghu.zhang
 */
public class BasicField {
	
	//DBObjectContext
	private static final Map<String,Document> Context = new HashMap<>();

	/**
	 * 获取field
	 * @param clz
	 * @return
	 */
	public static Document getDBObject(Class<?> clz) {
		
		Document dbObject = Context.get(clz.getClass().getName());
		
		if(Objects.isNull(dbObject)) {
			dbObject = new Document();
			Field[] supers = clz.getSuperclass().getDeclaredFields();
			Field[] fields = clz.getDeclaredFields();
			
			for (Field field : fields) {
				String name = field.getName();
				dbObject.put(name, 1);
			}
			
			for (Field field : supers) {
				String name = field.getName();
				dbObject.put(name, 1);
			}
			Context.put(clz.getClass().getName(), dbObject);
		}
		
		return dbObject;
	}
}
