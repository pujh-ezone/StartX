package com.startx.http.system.model;

/**
 * 三方接口调用凭证
 * 
 * @author minghu.zhang
 */
public class ApiToken {
	/**
	 * token值
	 */
	private String access_token;
	/**
	 * token过期时间
	 */
	private long expires_in;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public boolean isExpires() {

		if (System.currentTimeMillis() >= expires_in - 10000) {
			return true;
		}

		return false;
	}
}
