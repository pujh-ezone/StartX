package com.startx.http.system.encrypt.md5;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

public class MD5 {

	private static final Logger Log4j = Logger.getLogger(MD5.class);

	protected static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public final static String encrypt(String value) {
		try {
			byte[] btInput = value.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			Log4j.error(e.getMessage());
			return null;
		}
	}
	
	public final static String encrypt16(String value) {
		return encrypt(value).substring(8,24);
	}

}
