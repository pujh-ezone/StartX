package com.startx.http.system.tool;

/**
 * 文本工具
 * @author minghu.zhang
 */
public class TextTool {
	
	/**
	 * 正则
	 */
	private static final String REG = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？|\\d|A-Z|a-z]";
	/**
	 * 空串
	 */
	private static final String EMPTY = "";
	/**
	 * 段落结束标记
	 */
	private static final String PARAGRAPH = "</p>";
	
	/**
     * 全角转半角
     */
    private static String ToDBC(String text) {
        char c[] = text.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == '\u3000') {
                c[i] = ' ';
            } else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
                c[i] = (char) (c[i] - 65248);

            }
        }
        return new String(c);
    }
    
    /**
     * 删除文本字符，数字，字母
     */
    public static String cleanText(String text) {
    	return ToDBC(text).replaceAll(REG, EMPTY);
    }

    /**
     * 截取 size*10% 内容
     */
	public static String splitText(String content, int size) {
		StringBuffer buffer = new StringBuffer();
		int cursor = (content.length()*size*10)/100;
		buffer.append(content.substring(0,cursor));
		buffer.append("...");
		return buffer.toString();
	}
	
	/**
	 * 获取前三段落文本
	 * @param content
	 * @param length
	 * @return
	 */
	public static String getParagraph(String content, int size) {
		StringBuffer buffer = new StringBuffer();
		
		int paragraph = 0;
		for(;content.indexOf(PARAGRAPH) != -1 && paragraph < size;) {
			int cursor = content.indexOf(PARAGRAPH)+PARAGRAPH.length();
			buffer.append(content.substring(0, cursor));
			content = content.substring(cursor);
			paragraph++;
		}
		
		return buffer.toString();
	}
}
