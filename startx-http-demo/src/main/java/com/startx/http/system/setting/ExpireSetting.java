package com.startx.http.system.setting;

import com.startx.core.tools.ConfigTool;

/**
 * 过期时间配置
 * 
 * @author minghu.zhang
 */
public class ExpireSetting {

	/**
	 * 单例配置对象
	 */
	private static ExpireSetting expireSetting;

	static {
		expireSetting = new ConfigTool().read("/config.properties", ExpireSetting.class);
	}

	/**
	 * 短信验证码过期时间（分钟）
	 */
	private int smsExpire = 5;
	/**
	 * Token过期时间（小时）
	 */
	private int tokenExpire = 3;
	/**
	 * 随机验证值过期（分钟）
	 */
	private int nonceExpire = 30;

	public static ExpireSetting getExpireSetting() {
		return expireSetting;
	}

	public int getSmsExpire() {
		return smsExpire;
	}

	public void setSmsExpire(int smsExpire) {
		this.smsExpire = smsExpire;
	}

	public int getTokenExpire() {
		return tokenExpire;
	}

	public void setTokenExpire(int tokenExpire) {
		this.tokenExpire = tokenExpire;
	}

	public int getNonceExpire() {
		return nonceExpire;
	}

	public void setNonceExpire(int nonceExpire) {
		this.nonceExpire = nonceExpire;
	}
}
