package com.startx.http.system.setting;

import com.startx.core.tools.ConfigTool;

/**
 * AkSk配置
 * 
 * @author minghu.zhang
 */
public class SmsSetting {

	/**
	 * SMS 配置
	 */
	private static SmsSetting smsConfig;

	static {
		smsConfig = new ConfigTool().read("/config.properties", SmsSetting.class);
	}

	/**
	 * ak
	 */
	private String smsAk;
	/**
	 * sk
	 */
	private String smsSk;

	public static SmsSetting getSmsConfig() {
		return smsConfig;
	}

	public String getSmsAk() {
		return smsAk;
	}

	public void setSmsAk(String smsAk) {
		this.smsAk = smsAk;
	}

	public String getSmsSk() {
		return smsSk;
	}

	public void setSmsSk(String smsSk) {
		this.smsSk = smsSk;
	}
}
