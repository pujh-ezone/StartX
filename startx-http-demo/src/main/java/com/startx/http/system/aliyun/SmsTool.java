package com.startx.http.system.aliyun;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.startx.http.system.constant.Constants.SmsConf;
import com.startx.http.system.encrypt.aes.AES;
import com.startx.http.system.setting.SecureSetting;
import com.startx.http.system.setting.SmsSetting;

/**
 * 短信发送工具
 */
public class SmsTool {

	/**
	 * 产品名称域名
	 */
    private static final String product = "Dysmsapi";
    private static final String domain = "dysmsapi.aliyuncs.com";
    /**
     * 密钥配置
     */
    private static final SecureSetting secureConfig = SecureSetting.getSecureConfig();
    /**
     * 短信配置
     */
    private static final SmsSetting smsConfig = SmsSetting.getSmsConfig();
    private static final String aesSms = secureConfig.getAesSms();
    private static final String accessKeyId = AES.decrypt(smsConfig.getSmsAk(), aesSms);
    private static final String accessKeySecret = AES.decrypt(smsConfig.getSmsSk(), aesSms);

    //发送短信
    public static SendSmsResponse send(SmsConf sms,String telephone,String code) {
    	
    	SendSmsResponse sendSmsResponse = null;
		try {
			
	        //可自助调整超时时间
	        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
	        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
	
	        //初始化acsClient,暂不支持region化
	        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
	        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
	        IAcsClient acsClient = new DefaultAcsClient(profile);
	
	        //组装请求对象-具体描述见控制台-文档部分内容
	        SendSmsRequest request = new SendSmsRequest();
	        //必填:待发送手机号
	        request.setPhoneNumbers(telephone);
	        request.setSignName(sms.getSign());
	        request.setTemplateCode(sms.getCode());
	        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
	        request.setTemplateParam("{\"code\":\""+code+"\"}");
	
	        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
	        //request.setSmsUpExtendCode("90997");
	
	        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
	    	//request.setOutId("yourOutId");
	
	        //hint 此处可能会抛出异常，注意catch
        
			sendSmsResponse = acsClient.getAcsResponse(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

        return sendSmsResponse;
    }
}
