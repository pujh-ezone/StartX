package com.startx.http.system.datasource;

import org.apache.commons.dbcp.BasicDataSource;

import com.startx.http.system.encrypt.aes.AES;
import com.startx.http.system.setting.SecureSetting;

/**
 * 设置数据源
 * @author minghu.zhang
 */
public class ProjectDataSource extends BasicDataSource {
	
	private String aesPwd = SecureSetting.getSecureConfig().getAesDb();
	
	@Override
	public void setUsername(String username) {
		super.setUsername(AES.decrypt(username, aesPwd));
	}
	
	@Override
	public void setPassword(String password) {
		super.setPassword(AES.decrypt(password, aesPwd));
	}
}
