package com.startx.http.system.log;

import org.apache.log4j.Logger;

/**
 * Log4j工具类
 * @author minghu.zhang
 */
public class Log4j {
	/**
	 * 获取RootLogger
	 */
	private static final Logger Log = Logger.getRootLogger();
//	PropertyConfigurator.configure("conf/log4j.properties");
	public static void info(String message) {
		Log.info(message);
	}
	
	public static void info(String message,Throwable t) {
		Log.info(message, t);
	}
	
	public static void debug(String message) {
		Log.debug(message);
	}
	
	public static void debug(String message,Throwable t) {
		Log.debug(message, t);
	}
	
	public static void error(String message) {
		Log.error(message);
	}
	
	public static void error(String message,Throwable t) {
		Log.error(message, t);
	}
	
	public static void fatal(String message) {
		Log.fatal(message);
	}
	
	public static void fatal(String message,Throwable t) {
		Log.fatal(message, t);
	}
	
}

