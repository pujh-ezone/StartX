package com.startx.http.system.encrypt.sha1;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class Sha1 {
	
	public static String hash(String value) {
		MessageDigest sha1 = DigestUtils.getSha1Digest();
		return new String(Base64.encodeBase64(sha1.digest(value.getBytes())));
	}
	
}
