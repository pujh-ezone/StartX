package com.startx.http.system.tool;

import java.util.UUID;

import com.startx.http.system.encrypt.sha1.Sha1;

/**
 * Token生成器
 * @author minghu.zhang
 */
public class Token {
	
	/**
	 * 获取token
	 * @return
	 */
	public static final String get() {
		return Sha1.hash(UUID.randomUUID().toString());
	}
	
}
