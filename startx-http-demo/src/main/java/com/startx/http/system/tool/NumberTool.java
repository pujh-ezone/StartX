package com.startx.http.system.tool;

import java.math.BigDecimal;

public class NumberTool {
	
	/**
	 * 四舍五入
	 * @param value 原值
	 * @param scale 保留小数位
	 * @return
	 */
	public static double halfAdjust(double value,int scale) {
		BigDecimal   decimal   =   new   BigDecimal(value);  
		return   decimal.setScale(scale,   BigDecimal.ROUND_HALF_UP).doubleValue();  
	}
	
}
