package com.startx.http.system.cache;


import com.startx.core.mvc.ApplicationContext;
import com.startx.http.service.redis.RedisService;
import com.startx.http.system.model.TokenPair;
import com.startx.http.system.setting.ExpireSetting;
import com.startx.http.system.tool.RandomStr;


/**
 * Token缓存
 * @author minghu.zhang
 */
public class TokenCache {

	private static RedisService redisService = (RedisService) ApplicationContext.get().getBean("redisService");
	/**
	 * 系统配置
	 */
	private static ExpireSetting setting = ExpireSetting.getExpireSetting();

	/**
	 * 异常文本模板
	 */
//	private static ResourceManager ERORR = TextTemplate.ERORR;
	/**
	 * 过期时间
	 */
	private static int timeout = setting.getTokenExpire() * 60 * 60;

	/**
	 * 延长有效期
	 * @param token
	 */
	public static void delayExpire(String token) {
//		String userId = redisService.getString(token);
//
//		if(!Objects.isNull(userId)) {
//			String lastToken = redisService.getString(userId);
//			if(!Objects.isNull(lastToken) && token.equals(lastToken)) {
//				Log4j.info("token\tdelay token expire time...");
//				setTokenPair(new TokenPair(token, Long.valueOf(userId)));
//			}
//		}
	}
	
	/**
	 * 获取用户ID
	 * @param token
	 * @return
	 */
	public static String getUserId(String token) {
		
		if(token == null || "".equals(token)) {
			return "token";
		}
		
		return token;
		
//		if(Objects.isNull(token)) {
//			throwLoginExpireException();
//		}
//		
//		String userId = redisService.getString(token);
//
//		if(Objects.isNull(userId)) {
//			throwLoginExpireException();
//		}
//
//		String lastToken = redisService.getString(userId);
//
//		if(Objects.isNull(lastToken)
//				|| !token.equals(lastToken)) {
//			throwLoginExpireException();
//		}
//
//		return Long.valueOf(userId);
	}



	/**
	 * 验证Token
	 * @param token
	 */
	public static void checkLogin(String token) {
//		
//		
//		if(Objects.isNull(token)) {
//			throwLoginExpireException();
//		}
//		String userId = redisService.getString(token);
//		
//		if(Objects.isNull(userId)) {
//			throwLoginExpireException();
//		}
//		String lastToken = redisService.getString(userId);
//		
//		if(Objects.isNull(lastToken)
//				|| !token.equals(lastToken)) {
//			throwLoginExpireException();
//		}
	}

	/**
	 * 生成token
	 * @param userId
	 * @return
	 */
	public static String getToken(long userId){
		String token = RandomStr.getRandomStr(32,-1);
		TokenPair tokenPair = new TokenPair(token,userId);
		setTokenPair(tokenPair);
		return token;
	}
	
	/**
	 * 设置TokenPair
	 * @param pair
	 */
	public static void setTokenPair(TokenPair pair) {
		String userId = String.valueOf(pair.getUserId());
		redisService.saveString(pair.getToken(),userId,timeout);
		redisService.saveString(userId,pair.getToken(),timeout);
	}

	/**
	 * 抛出登录过期错误异常
	 */
	/*private static void throwLoginExpireException() {
		ResponseBuilder.Code timeout = ResponseBuilder.Code.TIMEOUT;
		timeout.setMsg(ERORR.getString("LoginInfoExpire"));
		throw new MiniException(timeout);
	}*/
	
}
