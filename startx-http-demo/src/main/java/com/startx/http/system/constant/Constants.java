package com.startx.http.system.constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.startx.http.system.tool.ResourceManager;

/**
 * 系统常量
 * 
 * @author minghu.zhang
 */
public interface Constants {

	/**
	 * 机器标识
	 */
	public static final String MACHINE_TAG = "A";
	/**
	 * 正斜杠
	 */
	public static final String FORWARD_SLASH = "/";
	/**
	 * 空串
	 */
	public static final String EMPTY_STRING = "";
	/**
	 * 未知文件目录
	 */
	public static final String FILE_CATALOG = "file";
	/**
	 * 点
	 */
	public static final String DOT = ".";
	/**
	 * sql文件目录
	 */
	public static final String SQL_MAPPER = "/mapper";
	/**
	 * 空列表
	 */
	public static final List<?> EMPTY_LIST = new ArrayList<>();
	/**
	 * 空对象
	 */
	public static final Map<String, Object> EMPTY_OBJECT = new HashMap<>();
	/**
	 * 空字典
	 */
	public static final Map<String, String> EMPTY_DICTIONARY = new HashMap<>();
	/**
	 * 默认分页大小
	 */
	public static final int DEFAULT_PAGE_SIZE = 10;
	/**
	 * 默认推荐条数
	 */
	public static final int DEFAULT_RECOMMEND_SIZE = 3;
	/**
	 * 默认页数
	 */
	public static final int DEFAULT_PAGE = 1;
	/**
	 * 敏感词间隔
	 */
	public static final int WORD_FILTER_DISTANCE = 0;
	/**
	 * 敏感词替换字符
	 */
	public static final char WORD_FILTER_REPLACE_SYMBOL = '*';
	/**
	 * 需加密信息存入mongodb前用aes加密的密码
	 */
	public static final String AES_PASSWORD = "password";
	/**
	 * 热门评论
	 */
	public static final int HOT_COMMENT_SIZE = 3;

	public enum UserStatus {
		/**
		 * 正常，禁止提现，禁止登录
		 */
		NORMAL, FORBIDDEN_GET_CASH, FORBIDDEN_LOGIN;
		private int type;

		private UserStatus() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 审核状态
	 * 
	 * @author minghu.zhang
	 */
	public enum AuditStatus {
		// 创建，成功，失败
		CREATE, SUCCESSFUL, FAILURE;
		private int type;

		private AuditStatus() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 订单状态
	 * 
	 * @author minghu.zhang
	 */
	public enum OrderStatus {
		// 创建订单，支付成功，支付失败，订单退款成功，订单退款失败
		CREATE, SUCCESSFUL, FAILURE, REFUND_SUCCESS, REFUND_FAILURE;
		private int type;

		private OrderStatus() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}


	/**
	 * 用户验证类型
	 * （注册、登录时入参类型）
	 */
	public enum UserVerifyType {
		/**
		 * 用户信息验证类型（0,1,2,3,4）
		 * 对应不同的权限
		 * 微博验证用户，qq验证用户，微信验证用户
		 */
		WEIBO_USER,QQ_USER,WECHAT_USER;
		private int type;

		private UserVerifyType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}

	}

	/**
	 * 微信商户配置
	 * 
	 * @author minghu.zhang
	 */
	public class WechatMch {
		/**
		 * 商户号
		 */
		public static final String MCH_ID = "";
		/**
		 * 调用密钥
		 */
		public static final String API_SECRET = "";
		/**
		 * 支付回调通知
		 */
		public static final String NOTIFY_URL = "";
		/**
		 * 商户证书位置
		 */
		public static final String KEYSTORY_PATH = "";
	}

	/**
	 * app配置
	 * 
	 * @author minghu.zhang
	 */
	public enum AppConfig {

		/**
		 * 小程序
		 */
		MINI_PROGRAM("", ""),
		/**
		 * 公众号
		 */
		PUBLIC_ACCOUNT("", "");
		/**
		 * appType编号
		 */
		private int type;
		/**
		 * app编号
		 */
		private String appId;
		/**
		 * app访问密码
		 */
		private String appSecret;

		private AppConfig(String appId, String appSecret) {
			this.type = this.ordinal();
			this.appId = appId;
			this.appSecret = appSecret;
		}

		/**
		 * 获取app配置
		 * 
		 * @param type
		 * @return
		 */
		public static AppConfig getAppConfig(int type) {
			return AppConfig.values()[type];
		}

		public int getType() {
			return type;
		}

		public String getAppId() {
			return appId;
		}

		public String getAppSecret() {
			return appSecret;
		}
	}

	/**
	 * 阿里云OSS
	 * 
	 * @author minghu.zhang
	 */
	public class Oss {
		public static String endpoint = "http://oss-cn-beijing-internal.aliyuncs.com";
		/**
		 * 访问域名
		 */
		// public static String endpoint = "http://oss-cn-beijing.aliyuncs.com";
		/**
		 * 账户ID
		 */
		public static String accessKeyId = "";
		/**
		 * 账户密钥
		 */
		public static String accessKeySecret = "";
		/**
		 * 空间名称
		 */
		public static String bucketName = "";
	}

	/**
	 * 阿里云OSS
	 * 
	 * @author minghu.zhang
	 */
	public enum SmsConf {
		REGISTERY("SMS_131050072","阿里云短信测试专用"), 
		FIND_PWD("SMS_131050072","阿里云短信测试专用"), 
		LOGIN("SMS_131050072","阿里云短信测试专用"), 
		VALIDATION("SMS_131050072","阿里云短信测试专用");
		// 短信编码
		private String code;
		// 短信签名
		private String sign;

		public String getCode() {
			return code;
		}

		public String getSign() {
			return sign;
		}

		private SmsConf(String code, String sign) {
			this.code = code;
			this.sign = sign;
		}
	}

	/**
	 * 文本模板常量
	 * 
	 * @author minghu.zhang
	 */
	public class TextTemplate {
		/**
		 * 异常模板
		 */
		public static final ResourceManager ERORR = ResourceManager.getInstance("i18n/error");
		/**
		 * 公共模板
		 */
		public static final ResourceManager COMMON = ResourceManager.getInstance("i18n/common");
	}

	/**
	 * 提现状态
	 * 
	 * @author minghu.zhang
	 */
	public enum PaymentStatus {
		// 创建订单，支付成功，支付失败
		CREATE, SUCCESSFUL, FAILURE;
		private int type;

		private PaymentStatus() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 排序类型状态
	 * 
	 * @author minghu.zhang
	 */
	public enum SortType {
		// 热度排序，时间排序
		HOT, DATE;
		private int type;

		private SortType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 剧本类型
	 * 
	 * @author minghu.zhang
	 */
	public enum StoryType {
		// AVG，动画
		AVG, ANIMATION;
		private int type;

		private StoryType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 长度类型
	 * 
	 * @author minghu.zhang
	 */
	public enum LengthType {
		// 爽剧，番剧
		SG, FG;
		private int type;

		private LengthType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 剧本类型
	 * 
	 * @author minghu.zhang
	 */
	public enum CommonStatus {
		// 草稿，发布，下架
		DRAFT, PUBLISH, REMOVE;
		private int type;

		private CommonStatus() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 剧本类型
	 * 
	 * @author minghu.zhang
	 */
	public enum SerialType {
		// 爽剧，番剧
		SG, FG;
		private int type;

		private SerialType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 操作类型
	 * 
	 * @author minghu.zhang
	 */
	public enum OpType {
		//
		CREATE, DELETE, UPDATE, QUERY;
		private int type;

		private OpType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 完结标志
	 * 
	 * @author minghu.zhang
	 */
	public enum EndFlag {
		// 更新中，已完结
		RUNNING, STOP;
		private int type;

		private EndFlag() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 内容类型
	 * 
	 * @author minghu.zhang
	 */
	public enum ContentType {
		// 剧本，剧集，评论
		STORY, STORY_SET, COMMENT;
		private int type;

		private ContentType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 举报类型
	 * 
	 * @author minghu.zhang
	 */
	public enum ReportType {
		// 剧本，剧集，评论
		STORY, STORY_SET, COMMENT, AUTHOR;
		private int type;

		private ReportType() {
			this.type = this.ordinal();
		}

		public int getType() {
			return type;
		}
	}

	/**
	 * 用户各个属性的默认值
	 */
	public class UserDefaultValue {

		/**
		 * UserBase
		 */
		//加V认证
		public static final int vType = 0;
		//经验值
		public static final long exp = 0l;

		/**
		 * UserData
		 */
		//关注数
		public static final long follow = 0l;
		//粉丝数
		public static final long fans = 0l;
		//点赞数
		public static final long like = 0l;
		//成就点数
		public static final long ach = 0l;

		/**
		 * UserIdentity
		 */
		//微博
		public static final String weibo = "";
		//qq
		public static final String qq = "";
		//微信
		public static final String wechat = "";
		//电话号码
		public static final String phone = "";
		//密码
		public static final String password = "";

		/**
		 * UserWallet
		 * 数据签名默认值需要在entity里生成（userId不同）
		 */
		//钻石数
		public static final int diamond = 0;
		//金币数
		public static final int gold = 0;
		//黄钻
		public static final int ydiamond = 0;

	}

	/**
	 * 每日签到获取物品类型
	 */
	public class DailyCheckInRewardTypeType {
		// 1 物品， 2 随机物品， 3 宝箱
		public static final int obj = 1;
		public static final int randomObj = 2;
		public static final int treasureChest = 3;
	}

	/**
	 * 日志类型
	 * @author minghu.zhang
	 */
	public enum LogType {
		AccessLog,UserCheckIn,UserLogin,DataUpdate,SystemException;
	}

}