package com.startx.http.system.setting;

import com.startx.core.tools.ConfigTool;

/**
 * 集群设置 
 * @author minghu.zhang
 */
public class ClusterSetting {

	/**
	 * 单例配置对象
	 */
	private static ClusterSetting clusterSetting;

	static {
		clusterSetting = new ConfigTool().read("/config.properties", ClusterSetting.class);
	}
	
	/**
	 * 集群ID
	 */
	private int clusterId;
	/**
	 * 服务ID
	 */
	private int serverId;

	public static ClusterSetting getClusterSetting() {
		return clusterSetting;
	}

	public int getClusterId() {
		return clusterId;
	}

	public void setClusterId(int clusterId) {
		this.clusterId = clusterId;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

}
