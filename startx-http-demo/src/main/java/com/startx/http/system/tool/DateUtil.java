package com.startx.http.system.tool;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 */
public class DateUtil {


	public static int getDaysBetweenDate(long long1,long long2){
		Date date1 = getTimeStr(long1);
		Date date2 = getTimeStr(long2);
		return differentDays(date1,date2);
	}


	/**
	 * date2比date1多的天数
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDays(Date date1,Date date2)
	{
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		int day1= cal1.get(Calendar.DAY_OF_YEAR);
		int day2 = cal2.get(Calendar.DAY_OF_YEAR);

		int year1 = cal1.get(Calendar.YEAR);
		int year2 = cal2.get(Calendar.YEAR);
		if(year1 != year2)   //同一年
		{
			int timeDistance = 0 ;
			for(int i = year1 ; i < year2 ; i ++)
			{
				if(i%4==0 && i%100!=0 || i%400==0)    //闰年
				{
					timeDistance += 366;
				}
				else    //不是闰年
				{
					timeDistance += 365;
				}
			}

			return timeDistance + (day2-day1) ;
		}
		else    //不同年
		{
			return day2-day1;
		}
	}



	public static Date getTimeStr(long time){
		SimpleDateFormat format =  new SimpleDateFormat( "yyyy-MM-dd" );
		String d = format.format(time);
		try {
			Date date=format.parse(d);
			return date;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}



	public static long date2Time(String time){
		SimpleDateFormat format =  new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		try{
			Date date = format.parse(time);
			return date.getTime();
		}catch (Exception e){
			e.printStackTrace();
			return 0l;
		}
	}
}
