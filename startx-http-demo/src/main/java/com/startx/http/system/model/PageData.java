package com.startx.http.system.model;

import java.util.List;

/**
 * 分页数据
 * @author minghu.zhang
 */
@SuppressWarnings("rawtypes")
public class PageData<T> {
	/**
	 * 数据列表长度
	 */
	private int pageSize;
	/**
	 * 总记录数
	 */
	private long totalRecord;
	/**
	 * 总页数
	 */
	private int totalPage;
	/**
	 * 数据列表
	 */
	private List<T> list;
	
	private PageData() {}
	
	public static PageData build() {
		return new PageData();
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
}
