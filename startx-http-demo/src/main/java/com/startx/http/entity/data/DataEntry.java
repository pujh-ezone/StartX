package com.startx.http.entity.data;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "data_entry")
public class DataEntry {

	@Id
	private String userId;
	/**
	 * 数据项
	 */
	private Map<String, ?> entry;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Map<String, ?> getEntry() {
		return entry;
	}

	public void setEntry(Map<String, ?> map) {
		this.entry = map;
	}

}
