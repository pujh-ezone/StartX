package com.startx.http.entity.common;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ag_sequence")
public class Sequence {

	/**
	 * 序列ID
	 */
	private long sequenceId;
	/**
	 * 集合名称
	 */
	@Id
	private String collName;

	public long getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(long sequenceId) {
		this.sequenceId = sequenceId;
	}

	public String getCollName() {
		return collName;
	}

	public void setCollName(String collName) {
		this.collName = collName;
	}

}