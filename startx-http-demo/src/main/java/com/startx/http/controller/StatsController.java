package com.startx.http.controller;

import org.springframework.stereotype.Controller;

import com.startx.core.mvc.define.RequestMapping;
import com.startx.core.mvc.define.RequestMethod;
import com.startx.core.system.param.HttpBody;
import com.startx.core.system.param.HttpParam;
import com.startx.http.system.annotation.RequireLogin;
import com.startx.http.system.exception.MiniException;
import com.startx.http.system.tool.ResponseBuilder;
import com.startx.http.system.tool.ResponseBuilder.HttpResponse;


@Controller
@RequestMapping("/stats")
public class StatsController {

    /**
     * 保存数据
     */
    @RequestMapping(value="/get",method={RequestMethod.GET})
    @RequireLogin
    public HttpResponse saveData(HttpParam param, HttpBody body) {
        try {
            return ResponseBuilder.build("timestamp", System.currentTimeMillis());
        } catch (MiniException e) {
            return ResponseBuilder.build(e.getCode());
        }
    }
}
