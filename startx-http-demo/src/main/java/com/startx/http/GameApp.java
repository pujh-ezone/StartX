package com.startx.http;

import com.startx.core.Bootstrap;

public class GameApp {
	
	public static void main(String[] args) throws Exception {
		Bootstrap.start();
	}
	
}