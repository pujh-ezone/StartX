package com.startx.http.test;

import com.startx.http.service.IRedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application.xml"})
public class RedisTestCase {
	@Inject
	private IRedisService redisService;

	@Test
	public void test() {
		redisService.saveString("keyString","valueString",50);
		redisService.saveSet("keySet","1","2");
		redisService.saveList("listKey","22222");
		Map<String,String> map = new HashMap<>();
		map.put("1","2");
		map.put("2","3");
		redisService.saveMap("toke",map);

		Map<String,String> map2 = new HashMap<>();
		map2.put("2","234");
		map2.put("3","321");
		redisService.saveMap("token",map2);

		System.out.println(redisService.getString("keyString"));
		System.out.println(redisService.getSet("keySet"));
		System.out.println(redisService.getList("listKey"));
		System.out.println(redisService.getMap("keyMap",""));
		System.out.println(redisService.getMap("keyMap","2"));
	}

}
