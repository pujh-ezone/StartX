package com.startx.http.test;

import org.springframework.data.annotation.Id;

public class Ter {
	
	@Id
	private int id;
	
	private String name;
	
	private double weight;
	
	private Tag[] tags;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Tag[] getTags() {
		return tags;
	}

	public void setTags(Tag[] tags) {
		this.tags = tags;
	}
}
