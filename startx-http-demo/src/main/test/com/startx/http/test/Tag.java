package com.startx.http.test;

import org.springframework.data.annotation.Id;

public class Tag {

	@Id
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Tag(String name) {
		super();
		this.name = name;
	}

}
